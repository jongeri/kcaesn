package com.ubuniworks.webapp.controller;

import com.ubuniworks.model.Comment;
import com.ubuniworks.model.Idea;
import com.ubuniworks.service.GenericManager;
import com.ubuniworks.service.IdeaManager;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/publish*")
public class publish extends BaseFormController {
    private IdeaManager ideaManager = null;
    private GenericManager<Comment, Integer> commentManager;

    public publish() {
        setCancelView("redirect:ideas");
        setSuccessView("redirect:ideas");
    }

    @Autowired
    public void setIdeaManager(IdeaManager ideaManager) {
        this.ideaManager = ideaManager;
    }

    @Autowired
    public void setCommentManager(GenericManager<Comment, Integer> commentManager) {
        this.commentManager = commentManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    protected String showForm(HttpServletRequest request)
            throws Exception {
        String ididea = request.getParameter("ididea");
        String action = request.getParameter("action");

        String success = getSuccessView();
        if (!StringUtils.isBlank(ididea)) {
            Idea idea = ideaManager.get(new Integer(ididea));
            if ("publish".equalsIgnoreCase(action)) {
                if (!allBusinessCanvasItemsPresent(idea)) {
                    saveError(request, "The business canvas has not been filled fully, please add missing items");
                } else {
                    idea.setPublished(true);
                    ideaManager.save(idea);
                    saveMessage(request, "The idea has been published successfully, it can now be seen in the I-share list");
                }
            } else if ("unpublish".equalsIgnoreCase(action)) {
                idea.setPublished(false);
                ideaManager.save(idea);
                saveMessage(request, "The idea has been unpublished successfully, it is now not visible in the I-share list");
            }
            success = "redirect:ideadisplay?ididea=" + idea.getIdidea() + "#details";
        }

        return success;
    }

    private boolean allBusinessCanvasItemsPresent(Idea idea) {
        if (ideaManager.getActivities(idea).isEmpty()) return false;
        if (ideaManager.getChannels(idea).isEmpty()) return false;
        if (ideaManager.getCostStructures(idea).isEmpty()) return false;
        if (ideaManager.getCustomerRelationships(idea).isEmpty()) return false;
        if (ideaManager.getCustomerSegments(idea).isEmpty()) return false;
        if (ideaManager.getRevenueStreams(idea).isEmpty()) return false;
        if (ideaManager.getValuePropositions(idea).isEmpty()) return false;
        if (ideaManager.getPartners(idea).isEmpty()) return false;
        if (ideaManager.getResources(idea).isEmpty()) return false;
        return true;
    }
}
