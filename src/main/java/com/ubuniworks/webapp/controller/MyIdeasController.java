package com.ubuniworks.webapp.controller;

import com.ubuniworks.service.IdeaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/myideas*")
public class MyIdeasController extends BaseFormController {
    private IdeaManager ideaManager = null;


    @Autowired
    public void setIdeaManager(IdeaManager ideaManager) {
        this.ideaManager = ideaManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Model handleRequest(@RequestParam(required = false, value = "q") String query)
            throws Exception {
        Model model = new ExtendedModelMap();

        model.addAttribute(ideaManager.getForUser(getUserUtil().getCurrentUser()));

        return model;
    }
}
