<%@ include file="/common/taglibs.jsp" %>
<head>
    <meta name="menu" content="UserMenu"/>
    <title><fmt:message key="userProfile.title"/></title>
     <link href="assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <style>
        /* uses font awesome for social icons */
        @import url(http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);

        .page-header {
            text-align: center;
        }

        /*social buttons*/
        .btn-social {
            color: white;
            opacity: 0.9;
        }

        .btn-social:hover {
            color: white;
            opacity: 1;
        }

        .btn-facebook {
            background-color: #3b5998;
            opacity: 0.9;
        }

        .btn-twitter {
            background-color: #00aced;
            opacity: 0.9;
        }

        .btn-linkedin {
            background-color: #0e76a8;
            opacity: 0.9;
        }

        .btn-github {
            background-color: #000000;
            opacity: 0.9;
        }

        .btn-google {
            background-color: #c32f10;
            opacity: 0.9;
        }

        .btn-stackoverflow {
            background-color: #D38B28;
            opacity: 0.9;
        }

        /* resume stuff */

        .bs-callout {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: #eee;
            border-image: none;
            border-radius: 3px;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            margin-bottom: 5px;
            padding: 20px;
        }

        .bs-callout:last-child {
            margin-bottom: 0px;
        }

        .bs-callout h4 {
            margin-bottom: 10px;
            margin-top: 0;
        }

        .bs-callout-danger {
            border-left-color: #d9534f;
        }

        .bs-callout-danger h4 {
            color: #d9534f;
        }

        .resume .list-group-item:first-child, .resume .list-group-item:last-child {
            border-radius: 0;
        }

        /*makes an anchor inactive(not clickable)*/
        .inactive-link {
            pointer-events: none;
            cursor: default;
        }

        .resume-heading .social-btns {
            margin-top: 15px;
        }

        .resume-heading .social-btns i.fa {
            margin-left: -5px;
        }

        @media (max-width: 992px) {
            .resume-heading .social-btn-holder {
                padding: 5px;
            }
        }

        /* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */

        .progress-bar {
            text-align: left;
            white-space: nowrap;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            cursor: pointer;
        }

        .progress-bar > .progress-type {
            padding-left: 10px;
        }

        .progress-meter {
            min-height: 15px;
            border-bottom: 2px solid rgb(160, 160, 160);
            margin-bottom: 15px;
        }

        .progress-meter > .meter {
            position: relative;
            float: left;
            min-height: 15px;
            border-width: 0px;
            border-style: solid;
            border-color: rgb(160, 160, 160);
        }

        .progress-meter > .meter-left {
            border-left-width: 2px;
        }

        .progress-meter > .meter-right {
            float: right;
            border-right-width: 2px;
        }

        .progress-meter > .meter-right:last-child {
            border-left-width: 2px;
        }

        .progress-meter > .meter > .meter-text {
            position: absolute;
            display: inline-block;
            bottom: -20px;
            width: 100%;
            font-weight: 700;
            font-size: 0.85em;
            color: rgb(160, 160, 160);
            text-align: left;
        }

        .progress-meter > .meter.meter-right > .meter-text {
            text-align: right;
        }


    </style>
</head>
    <div class="page-container">
        <div class="page-content-wrapper">
             <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Profile Overview
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                          <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                            <div class="btn-group btn-theme-panel">
                                     <a class="btn dropdown-toggle" href="<c:url value='/home'/>"><i class="icon-calendar icons"></i> Back to Dashboard</a> |
                                <a class="btn dropdown-toggle" href="<c:url value='logout'/>">
                                    Click here to Logout
                                    <i class="icon-power"></i>
                                </a>
                              
                            </div>
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
    <div class="page-content">
        <div class="container">
            <div class="page-content-inner">
       <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <div class="profile-sidebar">
                                        <!-- PORTLET MAIN -->
                                        <div class="portlet light profile-sidebar-portlet ">
                                            <!-- SIDEBAR USERPIC -->
                                            <div class="profile-userpic">
                                                <img src="assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt=""> </div>
                                            <!-- END SIDEBAR USERPIC -->
                                            <!-- SIDEBAR USER TITLE -->
                                            <div class="profile-usertitle">
                                                <div class="profile-usertitle-name"> Profile of ${user.fullName} </div>
                                                <div class="profile-usertitle-job"> Developer </div>
                                            </div>
                                            <!-- END SIDEBAR USER TITLE -->
                                            <!-- SIDEBAR BUTTONS -->
                                            <div class="profile-userbuttons">
                                                <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                                <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                            </div>
                                            <!-- END SIDEBAR BUTTONS -->
                                            <!-- SIDEBAR MENU -->
                                            <div class="profile-usermenu">
                                                <ul class="nav">
                                                    <li class="active">
                                                        <a href="<c:url value='/profile-overview'/>">
                                                            <i class="icon-home"></i> Overview </a>
                                                    </li>
                                                    <li>
                                                        <a href="<c:url value='/profile'/>">
                                                            <i class="icon-settings"></i> Account Settings </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="icon-info"></i> Help </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- END MENU -->
                                        </div>
                                        <!-- END PORTLET MAIN -->
                                        <!-- PORTLET MAIN -->
                                        <div class="portlet light ">
                                            <!-- STAT -->
                                            <div class="row-fluid list-separated profile-stat">
                                                <h4 class="profile-desc-title">My Contact Infomation</h4>
                                               <ul class="list-group">
                                        <li class="list-group-item profile-desc-link"> <i class="icon-user"></i> ${user.fullName}</li>
                                        <li class="list-group-item profile-desc-link"><i class="icon-briefcase"></i> Software Engineer</li>
                                        <li class="list-group-item profile-desc-link"><i class="fa fa-envelope-o"> </i> ${user.address.address} </li>
                                        <li class="list-group-item profile-desc-link"><i class="fa fa-phone"> </i> ${user.phoneNumber}
                                        </li>
                                        <li class="list-group-item profile-desc-link"><i class="fa fa-at"> </i> ${user.email}</li>
                                    </ul>
                                            </div>
                                          
                                        </div>
                                        <!-- END PORTLET MAIN -->
                                    </div>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                               <div class="row">
                                                <div class="col-md-12">
                                        <div class="portlet light ">
                                            <div class="portlet-title tabbable-line">
                                                <h4>Quick Links</h4>
                                            </div>
                                            <div class="portlet-body">
                                             <%@include file="profilesidebar.jsp" %>
                                            </div>
                                            </div>
                                        </div>
                                                </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                                        
                </div></div>
                            </div>
    </div>
</div>
</div>


<!-- Modal<%--#FEFA5A--%>-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 id="modaltitle"></h5>
            </div>
            <div class="modal-body" id="modalbody"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#addprojectbtn").click(function () {
            $("#modaltitle").html("Add Previous Project");
            $.ajax({
                url: '/pastprojectform?ajax=true&userid=${user.id}',
                success: function (data) {
                    $("#modalbody").html(data)
                },
                type: 'GET'
            });
        });
    });
    $(document).ready(function () {
        $("#addexperiencebtn").click(function () {
            $("#modaltitle").html("Add Experience");
            $.ajax({
                url: '/experienceform?ajax=true&userid=${user.id}',
                success: function (data) {
                    $("#modalbody").html(data)
                },
                type: 'GET'
            });
        });
    });
    $(document).ready(function () {
        $("#editsummarybtn").click(function () {
            tinymce.init({
                selector: '#usersummary',
                inline: true,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
            });
        });
    });
</script>