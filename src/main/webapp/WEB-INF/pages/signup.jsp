<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="signup.title"/></title>
</head>

<body class="signup"/>

<!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                     <!-- BEGIN CONTENT BODY-->
             <section class="login-container">
            <div class="container">
                <div class="row">
                <div class="col-sm-6">
                    <div class="icon-big"></div>
                    </div>
                    <div class="col-sm-6">
 <h4 class="title-small">Be Part of the Great</h4>
                        <h3 class="title-medium">Enterpreneurs Support Network</h3>
    <spring:bind path="user.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>
 <form:form commandName="user" method="post" action="signup" id="signupForm" autocomplete="off"
               cssClass="well" onsubmit="return validateSignup(this)">
       <h4 class="small-title-odd">Basic Infomation</h4>  
        <spring:bind path="user.username">
            <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
        <appfuse:label styleClass="sr-only" key="user.username"/>
            <div class="input-icon">
        <form:input cssClass="form-control" path="username" id="username" placeholder="Enter Preferred Username..." autofocus="true"/></div>
        <form:errors path="username" cssClass="help-block"/>
        </div>

        <div class="row">
            <spring:bind path="user.password">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="sr-only" key="user.password"/>
                <div class="input-icon">
                <form:password cssClass="form-control" path="password" placeholder="Enter your Password" id="password" showPassword="true"/></div>
                <form:errors path="password" cssClass="help-block"/>
            </div>
            <spring:bind path="user.passwordHint">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="sr-only" key="user.passwordHint"/>
                <div class="input-icon">
                <form:input cssClass="form-control" path="passwordHint" placeholder="Re-nter your Password" id="passwordHint"/></div>
                <form:errors path="passwordHint" cssClass="help-block"/>
            </div>
        </div>
        <div class="row">
            <spring:bind path="user.firstName">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="sr-only" key="user.firstName"/>
                <div class="input-icon">
                <form:input cssClass="form-control" path="firstName" placeholder="Enter Your First Name..." id="firstName" maxlength="50"/></div>
                <form:errors path="firstName" cssClass="help-block"/>
            </div>
            <spring:bind path="user.lastName">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="sr-only" key="user.lastName"/>
                <div class="input-icon">
                <form:input cssClass="form-control" path="lastName" placeholder="Enter Your Last Name..." id="lastName" maxlength="50"/></div>
                <form:errors path="lastName" cssClass="help-block"/>
            </div>
        </div>
        <div class="row">
            <spring:bind path="user.email">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="sr-only" key="user.email"/>
                <div class="input-icon">
                <form:input cssClass="form-control" path="email" placeholder="Enter your Valid Email Address..." id="email"/></div>
                <form:errors path="email" cssClass="help-block"/>
            </div>
            <div class="col-sm-6 form-group">
                <appfuse:label styleClass="sr-only" key="user.phoneNumber"/>
                <div class="input-icon">
                    <form:input cssClass="form-control" path="phoneNumber" placeholder="Enter your working Phone Number..." id="phoneNumber"/></div>
            </div>
        </div>
        <div class="row">
     <div class="col-sm-12 form-group">
            <appfuse:label styleClass="sr-only" key="user.website"/>
         <div class="input-icon">
            <form:input cssClass="form-control" path="website" placeholder="Enter your Website, www or http://" id="website"/>
         </div>
        </div>
     </div>
        <div>
            <legend class="accordion-heading">
                <a data-toggle="collapse" class="btn btn-lg green m-icon-big" href="#collapse-address">Click to fill <fmt:message key="user.address.address"/> in the Next Page <i class="m-icon-big-swapdown m-icon-white"></i></a>
            </legend>
            <div id="collapse-address" class="accordion-body collapse">
                <div class="form-group">
                    <appfuse:label styleClass="sr-only" key="user.address.address"/>
                    <div class="input-icon">
                        <form:input cssClass="form-control" path="address.address" placeholder="Physical Address" id="address.address"/></div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <appfuse:label styleClass="sr-only" key="user.address.city"/>
                        <div class="input-icon">
                            <form:input cssClass="form-control" path="address.city" placeholder="City" id="address.city"/></div>
                    </div>
                    <div class="col-sm-3 form-group">
                        <appfuse:label styleClass="sr-only" key="user.address.province"/>
                        <div class="input-icon">
                            <form:input cssClass="form-control" path="address.province" placeholder="Province" id="address.province"/></div>
                    </div>
                    <div class="col-sm-3 form-group">
                        <appfuse:label styleClass="sr-only" key="user.address.postalCode"/>
                        <div class="input-icon">
                            <form:input cssClass="form-control" path="address.postalCode" placeholder="Enter your Postal Code" id="address.postalCode"/></div>
                    </div>
                </div>
                <div class="form-group">
                    <appfuse:label styleClass="sr-only" key="user.address.country"/>
                    <div class="input-icon">
                    <appfuse:country name="address.country" prompt="" default="${user.address.country}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.register"/>
            </button>
            <button type="submit" class="btn btn-default" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
            <div class="pull-right">
            <p>You dont have an Account yet?<br/><a href="<c:url value="/login"/>">Create an Account here</a></p>
            </div>
        </div>
    </form:form>
   
</div>

<c:set var="scripts" scope="request">
    <v:javascript formName="signup" staticJavascript="false"/>
    <script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>
</c:set>