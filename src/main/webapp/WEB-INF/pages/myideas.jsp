<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="ideaList.title"/></title>
    <meta name="menu" content="IdeaMenu"/>
     <link href="assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
</head>
 <body class="page-container-bg-solid">
  <div class="page-container">
        <div class="page-content-wrapper">
 <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>My Ideas
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                            <div class="btn-group btn-theme-panel">
                                <a class="btn dropdown-toggle" href="<c:url value='logout'/>">
                                    Click here to Logout
                                    <i class="icon-power"></i>
                                </a>
                              
                            </div>
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
<%--<display:table name="ideaList" class="table table-condensed table-striped table-hover" requestURI="" id="ideaList"--%>
<%--export="true" pagesize="25">--%>
<%--<display:column property="ididea" sortable="true" href="ideaform" media="html"--%>
<%--paramId="ididea" paramProperty="ididea" titleKey="idea.ididea"/>--%>
<%--<display:column property="ididea" media="csv excel xml pdf" titleKey="idea.ididea"/>--%>
<%--<display:column title="Actions"><a class="btn btn-xs btn-link"--%>
<%--href="ideadisplay/?ididea=${idea.ididea}">Details</a></display:column>--%>
<%--<display:column property="title" sortable="true" titleKey="idea.title"/>--%>
<%--<display:column property="description" sortable="true" titleKey="idea.description"/>--%>

<%--<display:setProperty name="paging.banner.item_name"><fmt:message key="ideaList.idea"/></display:setProperty>--%>
<%--<display:setProperty name="paging.banner.items_name"><fmt:message key="ideaList.ideas"/></display:setProperty>--%>

<%--<display:setProperty name="export.excel.filename"><fmt:message key="ideaList.title"/>.xls</display:setProperty>--%>
<%--<display:setProperty name="export.csv.filename"><fmt:message key="ideaList.title"/>.csv</display:setProperty>--%>
<%--<display:setProperty name="export.pdf.filename"><fmt:message key="ideaList.title"/>.pdf</display:setProperty>--%>
<%--</display:table>--%>
 <div class="page-content">
        <div class="container">
            <div class="page-content-inner">
                <div class="inbox">
<div class="row">
    <div class="col-sm-3">
        <%@include file="ideasidebar.jsp" %>
    </div>
    <div class="col-sm-9">
         <div class="todo-content">
            <div class="portlet light ">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <h3>My Ideas</h3>

                    <p><fmt:message key="ideaList.message"/></p>
                </div>
                <div>
                   
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <display:table name="ideaList" class="table table-hover"
                               requestURI=""
                               id="ideaList" export="true" pagesize="25">
                    <display:column property="title" sortable="true" href="ideadisplay" media="html"
                                    paramId="ididea" paramProperty="ididea" titleKey="idea.title"/>
                    <display:column property="ididea" media="csv excel xml pdf" titleKey="idea.ididea"/>
                    <display:column property="title" media="csv excel xml pdf" titleKey="idea.title"/>
                    <display:column property="description" sortable="true" titleKey="idea.description"/>
                    <display:column property="datecreated" sortable="true" titleKey="idea.datecreated"/>

                    <display:setProperty name="paging.banner.item_name"><fmt:message
                            key="ideaList.idea"/></display:setProperty>
                    <display:setProperty name="paging.banner.items_name"><fmt:message
                            key="ideaList.ideas"/></display:setProperty>

                    <display:setProperty name="export.excel.filename"><fmt:message
                            key="ideaList.title"/>.xls</display:setProperty>
                    <display:setProperty name="export.csv.filename"><fmt:message
                            key="ideaList.title"/>.csv</display:setProperty>
                    <display:setProperty name="export.pdf.filename"><fmt:message
                            key="ideaList.title"/>.pdf</display:setProperty>
                </display:table></div>
        </div>

        <%--<c:forEach items="${ideaList}" var="idea" varStatus="loop">--%>
        <%--<div class="panel panel-default">--%>
        <%--<div class="panel-heading">--%>
        <%--<a href="ideadisplay?ididea=${idea.ididea}">--%>
        <%--<h5>${idea.title}</h5>--%>
        <%--</a>--%>
        <%--</div>--%>
        <%--<div class="panel-body">--%>
        <%--<p>--%>
        <%--${idea.description}--%>
        <%--</p>--%>
        <%--</div>--%>
        <%--<div class="panel-footer"></div>--%>
        <%--</div>--%>
        <%--</c:forEach>--%>
    </div>
</div> </div>
</div>
                     </div>
    </div>
</div>
</div>
</div>
</div>
</body>