<%@ include file="/common/taglibs.jsp" %>

<head>
    <title><fmt:message key="userProfile.title"/></title>
    <meta name="menu" content="UserMenu"/>
    <link href="assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
</head>
 <body class="page-container-bg-solid">
  <div class="page-container">
        <div class="page-content-wrapper">
            <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Ideas
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                            <div class="btn-group btn-theme-panel">
                                     <a class="btn dropdown-toggle" href="<c:url value='/home'/>"><i class="icon-calendar icons"></i> Back to Dashboard</a> |
                                <a class="btn dropdown-toggle" href="<c:url value='logout'/>">
                                    Click here to Logout
                                    <i class="icon-power"></i>
                                </a>
                              
                            </div>
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
             <div class="page-content">
        <div class="container">
            <div class="page-content-inner">
                <div class="inbox">
<c:set var="delObject" scope="request"><fmt:message key="userList.user"/></c:set>
<script type="text/javascript">var msgDelConfirm =
        "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
</script>
<div class="row">
<div class="col-md-12">
     <!-- BEGIN PROFILE SIDEBAR -->
                                    <div class="profile-sidebar">
                                        <!-- PORTLET MAIN -->
                                        <div class="portlet light profile-sidebar-portlet ">
                                            <!-- SIDEBAR USERPIC -->
                                            <div class="profile-userpic">
                                                <img src="assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt=""> </div>
                                            <!-- END SIDEBAR USERPIC -->
                                            <!-- SIDEBAR USER TITLE -->
                                            <div class="profile-usertitle">
                                                <div class="profile-usertitle-name"> Profile of ${user.fullName} </div>
                                                <div class="profile-usertitle-job"> Developer </div>
                                            </div>
                                            <!-- END SIDEBAR USER TITLE -->
                                            <!-- SIDEBAR BUTTONS -->
                                            <div class="profile-userbuttons">
                                                <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                                <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                            </div>
                                            <!-- END SIDEBAR BUTTONS -->
                                            <!-- SIDEBAR MENU -->
                                            <div class="profile-usermenu">
                                                <ul class="nav">
                                                    <li class="active">
                                                        <a href="<c:url value='/profile-overview'/>">
                                                            <i class="icon-home"></i> Overview </a>
                                                    </li>
                                                    <li>
                                                        <a href="<c:url value='/profile'/>">
                                                            <i class="icon-settings"></i> Account Settings </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">
                                                            <i class="icon-info"></i> Help </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- END MENU -->
                                        </div>
                                        <!-- END PORTLET MAIN -->
                                        <!-- PORTLET MAIN -->
                                        <div class="portlet light ">
                                            <!-- STAT -->
                                            <div class="row-fluid list-separated profile-stat">
                                                <h4 class="profile-desc-title">My Contact Infomation</h4>
                                               <ul class="list-group">
                                        <li class="list-group-item profile-desc-link"> <i class="icon-user"></i> ${user.fullName}</li>
                                        <li class="list-group-item profile-desc-link"><i class="icon-briefcase"></i> Software Engineer</li>
                                        <li class="list-group-item profile-desc-link"><i class="fa fa-envelope-o"> </i> ${user.address.address} </li>
                                        <li class="list-group-item profile-desc-link"><i class="fa fa-phone"> </i> ${user.phoneNumber}
                                        </li>
                                        <li class="list-group-item profile-desc-link"><i class="fa fa-at"> </i> ${user.email}</li>
                                    </ul>
                                            </div>
                                          
                                        </div>
    </div>
                                        <!-- END PORTLET MAIN -->
                                        <div class="profile-content">
                                             <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title tabbable-line">
                <h2><fmt:message key="userProfile.heading"/></h2>
            <c:choose>
                <c:when test="${param.from == 'list'}">
                    <p><fmt:message key="userProfile.admin.message"/></p>
                </c:when>
                <c:otherwise>
                    <p><fmt:message key="userProfile.message"/></p>
                </c:otherwise>
            </c:choose>
                </div>
            
       
   <div class="portlet-body">
    <spring:bind path="user.*">
        <c:if test="${not empty status.errorMessages}">
            <div class="alert alert-danger alert-dismissable">
                <a href="#" data-dismiss="alert" class="close">&times;</a>
                <c:forEach var="error" items="${status.errorMessages}">
                    <c:out value="${error}" escapeXml="false"/><br/>
                </c:forEach>
            </div>
        </c:if>
    </spring:bind>

    <form:form commandName="user" method="post" action="userform" id="userForm" autocomplete="off"
               cssClass="" onsubmit="return validateUser(this)">
        <form:hidden path="id"/>
        <form:hidden path="version"/>
        <input type="hidden" name="from" value="<c:out value="${param.from}"/>"/>

        <spring:bind path="user.username">
            <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
        <appfuse:label styleClass="control-label" key="user.username"/>
        <form:input cssClass="form-control" path="username" id="username"/>
        <form:errors path="username" cssClass="help-block"/>
        <c:if test="${pageContext.request.remoteUser == user.username}">
                <span class="help-block">
                    <a href="<c:url value="/updatePassword" />"><fmt:message
                            key='updatePassword.changePasswordLink'/></a>
                </span>
        </c:if>
        </div>

        <spring:bind path="user.passwordHint">
            <div class="form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
        </spring:bind>
        <appfuse:label styleClass="control-label" key="user.passwordHint"/>
        <form:input cssClass="form-control" path="passwordHint" id="passwordHint"/>
        <form:errors path="passwordHint" cssClass="help-block"/>
        </div>

        <div class="row">
            <spring:bind path="user.firstName">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="control-label" key="user.firstName"/>
                <form:input cssClass="form-control" path="firstName" id="firstName" maxlength="50"/>
                <form:errors path="firstName" cssClass="help-block"/>
            </div>
            <spring:bind path="user.lastName">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="control-label" key="user.lastName"/>
                <form:input cssClass="form-control" path="lastName" id="lastName" maxlength="50"/>
                <form:errors path="lastName" cssClass="help-block"/>
            </div>
        </div>
        <div class="row">
            <spring:bind path="user.email">
            <div class="col-sm-6 form-group${(not empty status.errorMessage) ? ' has-error' : ''}">
                </spring:bind>
                <appfuse:label styleClass="control-label" key="user.email"/>
                <form:input cssClass="form-control" path="email" id="email"/>
                <form:errors path="email" cssClass="help-block"/>
            </div>
            <div class="col-sm-6 form-group">
                <appfuse:label styleClass="control-label" key="user.phoneNumber"/>
                <form:input cssClass="form-control" path="phoneNumber" id="phoneNumber"/>
            </div>
        </div>
        <div class="form-group">
            <appfuse:label styleClass="control-label" key="user.website"/>
            <form:input cssClass="form-control" path="website" id="website"/>
        </div>
        <div>
            <legend class="accordion-heading">
                <a data-toggle="collapse" class="btn btn-lg green m-icon-big" href="#collapse-address">Click to fill <fmt:message key="user.address.address"/> in the Next Page <i class="m-icon-big-swapdown m-icon-white"></i></a>
            </legend>
            <div id="collapse-address" class="accordion-body collapse">
                <div class="form-group">
                    <appfuse:label styleClass="control-label" key="user.address.address"/>
                    <form:input cssClass="form-control" path="address.address" id="address.address"/>
                </div>
                <div class="row">
                    <div class="col-sm-7 form-group">
                        <appfuse:label styleClass="control-label" key="user.address.city"/>
                        <form:input cssClass="form-control" path="address.city" id="address.city"/>
                    </div>
                    <div class="col-sm-2 form-group">
                        <appfuse:label styleClass="control-label" key="user.address.province"/>
                        <form:input cssClass="form-control" path="address.province" id="address.province"/>
                    </div>
                    <div class="col-sm-3 form-group">
                        <appfuse:label styleClass="control-label" key="user.address.postalCode"/>
                        <form:input cssClass="form-control" path="address.postalCode" id="address.postalCode"/>
                    </div>
                </div>
                <div class="form-group">
                    <appfuse:label styleClass="control-label" key="user.address.country"/>
                    <appfuse:country name="address.country" prompt="" default="${user.address.country}"/>
                </div>
            </div>
        </div>
        <c:choose>
            <c:when test="${param.from == 'list' or param.method == 'Add'}">
                <div class="form-group">
                    <label class="control-label"><fmt:message key="userProfile.accountSettings"/></label>
                    <label class="checkbox-inline">
                        <form:checkbox path="enabled" id="enabled"/>
                        <fmt:message key="user.enabled"/>
                    </label>

                    <label class="checkbox-inline">
                        <form:checkbox path="accountExpired" id="accountExpired"/>
                        <fmt:message key="user.accountExpired"/>
                    </label>

                    <label class="checkbox-inline">
                        <form:checkbox path="accountLocked" id="accountLocked"/>
                        <fmt:message key="user.accountLocked"/>
                    </label>

                    <label class="checkbox-inline">
                        <form:checkbox path="credentialsExpired" id="credentialsExpired"/>
                        <fmt:message key="user.credentialsExpired"/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="userRoles" class="control-label"><fmt:message key="userProfile.assignRoles"/></label>
                    <select id="userRoles" name="userRoles" multiple="true" class="form-control">
                        <c:forEach items="${availableRoles}" var="role">
                            <option value="${role.value}" ${fn:contains(user.roles, role.label) ? 'selected' : ''}>${role.label}</option>
                        </c:forEach>
                    </select>
                </div>
            </c:when>
            <c:when test="${not empty user.username}">
                <div class="form-group">
                    <label class="control-label"><fmt:message key="user.roles"/>:</label>

                    <div class="readonly">
                        <c:forEach var="role" items="${user.roleList}" varStatus="status">
                            <c:out value="${role.label}"/><c:if test="${!status.last}">,</c:if>
                            <input type="hidden" name="userRoles" value="<c:out value="${role.label}"/>"/>
                        </c:forEach>
                    </div>
                    <form:hidden path="enabled"/>
                    <form:hidden path="accountExpired"/>
                    <form:hidden path="accountLocked"/>
                    <form:hidden path="credentialsExpired"/>
                </div>
            </c:when>
        </c:choose>
        <div class="form-group">
            <button type="submit" class="btn btn-save" name="save" onclick="bCancel=false">
                <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
            </button>

            <c:if test="${param.from == 'list' and param.method != 'Add'}">
                <button type="submit" class="btn btn-default" name="delete"
                        onclick="bCancel=true;return confirmMessage(msgDelConfirm)">
                    <i class="icon-trash"></i> <fmt:message key="button.delete"/>
                </button>
            </c:if>

            <button type="submit" class="btn btn-default" name="cancel" onclick="bCancel=true">
                <i class="icon-remove"></i> <fmt:message key="button.cancel"/>
            </button>
        </div>
    </form:form> </div>
</div>
            </div>
                                        </div>
                                    </div>
    </div>
</div>
   

<c:set var="scripts" scope="request">
    <script type="text/javascript">
        // This is here so we can exclude the selectAll call when roles is hidden
        function onFormSubmit(theForm) {
            return validateUser(theForm);
        }
    </script>
</c:set>
</div>
</div>
    </div>
    </div>
</div>
</div>
</div>

<v:javascript formName="user" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

</body>